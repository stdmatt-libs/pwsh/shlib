##----------------------------------------------------------------------------##
## Constants                                                                  ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
$SCRIPT_FULLPATH = $MyInvocation.MyCommand.Path;
$PROJECT_DIR     = (Split-Path -Parent "${SCRIPT_FULLPATH}");
$LIB_DIR         = "${HOME}/.lib";             ## The location of our custom libraries. 
$INSTALL_DIR     = (Join-Path ${LIB_DIR} "shlib");

##----------------------------------------------------------------------------##
## Install                                                                    ##
##----------------------------------------------------------------------------##
##------------------------------------------------------------------------------
Write-Output "Installing [shlib]";
. "${PROJECT_DIR}/source/shlib.ps1"; ## source to use the shlib functions here...

sh_ensure_directory "${INSTALL_DIR}/shlib.ps1";
sh_mklink           "${PROJECT_DIR}/source/shlib.ps1" "${INSTALL_DIR}/shlib.ps1";

$colored = "$(sh_colored "cyan")${INSTALL_DIR}$(sh_colored)";
sh_log "shlib was installed at: (${colored})";
sh_log -fg "green" "Done...";

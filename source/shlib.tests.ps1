Describe "shlib" {
    ##
    ## sh_join_string
    ##

    ##--------------------------------------------------------------------------
    Context "sh_join_string" {
        BeforeAll {
            Import-Module ./shlib.psm1
        }

        It "no arguments" {
            (sh_join_string) | Should -Be "";
        }

        It "just the separator" {
            (sh_join_string "-") | Should -Be "";
        }

        It "empty string" {
            (sh_join_string "-" "") | Should -Be "";
        }

        It "multiple empty string" {
            (sh_join_string "-" "" "" "") | Should -Be "";
        }

        It "ignore whitespace" {
            (sh_join_string "-" " ") | Should -Be "";
        }

        It "ignore multiple whitespace" {
            (sh_join_string "-" " " " " " ") | Should -Be "";
        }

        It "ignore whitespace but maintain others" {
            (sh_join_string "-" "hello" " " "world") | Should -Be "hello-world";
        }

        It "empty separator" {
            (sh_join_string "" "hello" "world") | Should -Be "helloworld";
        }
    }

    ##--------------------------------------------------------------------------
    Context "sh_flatten_array" {
        It "null" {
            (sh_flatten_array $null) | Should -Be $null;
        }

        It "empty array " {
            $arr = @();
            (sh_flatten_array $arr) | Should -Be @();
        }

        It "flat array" {
            $arr = @(1, 2, 3);
            (sh_flatten_array $arr) | Should -Be @(1, 2, 3);
        }

        It "one level array" {
            $arr = @(1, 2, 3, @(4, 5));
            (sh_flatten_array $arr) | Should -Be @(1, 2, 3, 4, 5);
        }

        It "mulitple one level array" {
            $arr = @(1, 2, 3, @(4, 5), @(6));
            (sh_flatten_array $arr) | Should -Be @(1, 2, 3, 4, 5, 6);
        }

        It "multiple levels array" {
            $arr = @(1, 2, 3, @(4, @(5, 6, @(7), 8)), 9);
            (sh_flatten_array $arr) | Should -Be @(1, 2, 3, 4, 5, 6, 7, 8, 9);
        }
    }
}

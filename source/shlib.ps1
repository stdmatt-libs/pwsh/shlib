##----------------------------------------------------------------------------##
##                                                                            ##
## Array Utils                                                                ##
##                                                                            ##
##----------------------------------------------------------------------------##

##------------------------------------------------------------------------------
function sh_flatten_array($arr)
{
    $result = @();
    foreach($item in $arr) {
        $value = $item.ForEach({$_});
        $result += $value;
    }
    return $result;
}

##------------------------------------------------------------------------------
function sh_expand_array()
{
    $arr    = $args[0];
    $values = $arr.ForEach({$_});
    $start  = if($args.Length -gt 1) { $args[1]; } else { 0 }
    $end    = if($args.Length -gt 2) { $args[2]; } else { $values.Count }

    $values = $values[$start..$end];
    return $values;
}


##----------------------------------------------------------------------------##
##                                                                            ##
## Directory Management                                                       ##
##                                                                            ##
##----------------------------------------------------------------------------##

##------------------------------------------------------------------------------
function sh_push_dir()
{
    Push-Location $args[0];
}

##------------------------------------------------------------------------------
function sh_pop_dir()
{
    Pop-Location
}

##------------------------------------------------------------------------------
function sh_mkdir($path_to_create)
{
    if(-not (sh_dir_exists $path_to_create)) {
        $null = (New-Item -ItemType Directory -Path $path_to_create );
    }
}

##------------------------------------------------------------------------------
function sh_rmdir($path_to_remove)
{
    try {
        return (Remove-Item -Recurse -Force -Path $path_to_remove);
    } catch {
        return $false;
    }
}

##------------------------------------------------------------------------------
function sh_ensure_directory($some_path)
{
    return (sh_mkdir (sh_dirpath $some_path));
}

##------------------------------------------------------------------------------
function sh_mklink()
{
    $src = $args[0];
    $dst = $args[1];

    if((sh_is_unix_like)) {
        ln -f $src $dst;
    } else {
        New-Item -ItemType HardLink -Force -Target $src -Path $dst;
    }
}

##----------------------------------------------------------------------------##
##                                                                            ##
## File Utils                                                                 ##
##                                                                            ##
##----------------------------------------------------------------------------##

##------------------------------------------------------------------------------
function sh_write_file()
{
    $filename = $args[0];
    $content  = $args[1];

    Out-File -Filepath $filename -Encoding utf8 -Force -InputObject $content;
}


##----------------------------------------------------------------------------##
##                                                                            ##
## INI                                                                        ##
##                                                                            ##
##----------------------------------------------------------------------------##

##------------------------------------------------------------------------------
## Thanks to: Artem Tikhomirov - https://stackoverflow.com/a/422529
function sh_parse_ini_file($file)
{
    $ini = @{}

    # Create a default section if none exist in the file. Like a java prop file.
    $section = "NO_SECTION"
    $ini[$section] = [ordered]@{}
    try {
        switch -regex -file $file {
            "^\[(.+)\]$" {
                $section = $matches[1].Trim()
                $ini[$section] = [ordered]@{}
            }
            "^\s*([^#].+?)\s*=\s*(.*)" {
                $name, $value = $matches[1..2]
                # skip comments that start with semicolon:
                if (!($name.StartsWith(";"))) {
                    $ini[$section][$name] = $value.Trim()
                }
            }
        }
    } catch {
        return $null;
    }

    return $ini;
}

##------------------------------------------------------------------------------
function sh_print_ini($ini)
{
    $str = "";
    foreach($section_name in $ini.Keys) {
        $section = $ini[$section_name];
        if($section.Count -eq 0) {
            continue;
        }

        $str += "[$section_name]" + $SH_NEW_LINE;
        foreach($item_name in $section.Keys) {
            $item_value = $section[$item_name];
            $str += "   $item_name = $item_value".TrimEnd() + $SH_NEW_LINE;
        }
    }

    sh_echo $str;
}

##------------------------------------------------------------------------------
function sh_write_ini_to_file($ini, $filename)
{
    $str = (sh_print_ini $ini);
    (sh_write_file $filename $str);
}

## @todo(stdmatt): [Incomplete ini functions] at 22-03-06
##   - Add    value / section
##   - Modify value / section
##   - Create bare ini.
##------------------------------------------------------------------------------
function sh_ini_delete_section()
{
    $ini     = $args[0];
    $section = $args[1];
    $ini.Remove($section);
}

##------------------------------------------------------------------------------
function sh_ini_delete_value_on_section()
{
    $ini     = $args[0];
    $section = $args[1];
    $value   = $args[2];

    $ini[$section].Remove($value);
    return $ini;
}

##----------------------------------------------------------------------------##
##                                                                            ##
## Log Utils                                                                  ##
##                                                                            ##
##----------------------------------------------------------------------------##

##------------------------------------------------------------------------------
function _sh_log_get_call_function_name()
{
    $callstack = Get-PSCallStack;
    foreach($command in $callstack) {
        $function_name = $command.FunctionName;
        if($function_name.StartsWith("_")) {
            continue;
        }
        return $function_name;
   }

    return $function_name;
}

##------------------------------------------------------------------------------
function sh_log_fatal()
{
    $function_name = _sh_log_get_call_function_name;

    $output  = "$(sh_colored "red")[FATAL]$(sh_colored "gray")[${function_name}]$(sh_colored) ";
    $output += $args;

    sh_echo $output;
}

##------------------------------------------------------------------------------
function sh_log_verbose()
{
    if($env:SHLIB_IS_VERSBOSE -eq 1) {
        sh_log "$args";
    }
}

##------------------------------------------------------------------------------
function sh_log()
{
    param(
        $text,
        $fg,
        $bg
    );

    $function_name = (_sh_log_get_call_function_name);
    # $(sh_colored "#c2c2c2")[${function_name}]

    $colored_args = "$(sh_colored $fg $bg)$text$(sh_colored)";
    sh_echo "$colored_args";
}


##----------------------------------------------------------------------------##
##                                                                            ##
## OS Utils                                                                   ##
##                                                                            ##
##----------------------------------------------------------------------------##

##------------------------------------------------------------------------------
function sh_get_os_name()
{
    if((sh_is_wsl)) {
        return "wsl";
    } elseif($IsWindows) {
        return "win";
    } elseif($IsLinux) {
        return "gnu";
    } elseif($IsMacOS) {
        return "mac";
    }
    return "unsupported";
}

##------------------------------------------------------------------------------
function sh_is_windows_or_wsl() 
{
    return $IsWindows -or (sh_is_wsl);
}

##------------------------------------------------------------------------------
function sh_is_wsl()
{
    if($IsLinux) {
        $result = (uname -a);
        $index  = $result.IndexOf("WSL");
        if($index -eq -1) {
            return $false;
        }
        return $true;
    }
    return $false;
}

##------------------------------------------------------------------------------
function sh_is_unix_like()
{
    return (sh_is_wsl) -or $IsLinux -or $IsMacOS;
}


##----------------------------------------------------------------------------##
##                                                                            ##
## Path Utils                                                                 ##
##                                                                            ##
##----------------------------------------------------------------------------##

##------------------------------------------------------------------------------
function sh_abspath()
{
    Resolve-Path $args[0];
}

##------------------------------------------------------------------------------
function sh_basepath()
{
    $arg = $args[0];
    $arg = $arg.Replace("\", "/");
    if (-not $arg) {
        return "";
    }

    return $arg.Split("/")[-1];
}

##------------------------------------------------------------------------------
function sh_dirpath()
{
    $arg   = $args[0];
    $final = (Split-Path $arg -Parent);

    return $final;
}



##------------------------------------------------------------------------------
function sh_get_script_dir()
{
    return $MyInvocation.PSScriptRoot;
}

##------------------------------------------------------------------------------
function sh_get_script_path()
{
    return $MyInvocation.PSCommandPath;
}

##------------------------------------------------------------------------------
function sh_get_script_filename()
{
    return (sh_basepath $MyInvocation.PSCommandPath);
}


##------------------------------------------------------------------------------
function sh_get_home_dir()
{
    if ($HOME -eq "") {
        return "$env:USERPROFILE"
    }

    return $HOME;
}



##------------------------------------------------------------------------------
function sh_file_exists()
{
    if(-not $args[0]) {
        return $false;
    }
    return (Test-Path -Path $args[0] -PathType Leaf);
}

##------------------------------------------------------------------------------
function sh_dir_exists()
{
    if(-not ($args[0])) {
        return $false;
    }

    return (Test-Path -Path $args[0] -PathType Container);
}


##------------------------------------------------------------------------------
function sh_join_path()
{
    return [IO.Path]::Combine($args -split " ");
}




##----------------------------------------------------------------------------##
##                                                                            ##
## Prompt                                                                     ##
##                                                                            ##
##----------------------------------------------------------------------------##

##------------------------------------------------------------------------------
function sh_ask()
{
    Param(
        $Text,    ## Message that will appear
        $Default, ## Default value if enter empty...
        $Hint,    ## Value to be displayed as hint (if !placeholder)
        $Valid,   ## Array of valid inputs...

        [switch] $NoErrorHint,  ## Doesn't show the error message when input is invalid.
        [switch] $EmptyIsValid, ## Empty input is considered valid.
        [switch] $IgnoreCase    ## Letter case doesn't matters in valid values.
    );

    $erase_escape = (sh_ansi "0J");
    $up_escape    = (sh_ansi "1F");
    $reset_escape = (sh_ansi_color 0);

    ##
    ## Create the prompt strings...
    ##

    $valid_str = "";
    if($Valid -and $Valid.Count -ne 0) {
        $str       = (sh_join_string ", " $Valid);
        $valid_str = "$(sh_colored "darkgray")(${str})";
    }

    $hint_str  = "";
    if($Hint.Count -ne 0 -or $Default.Count -ne 0) {
        $str      = (sh_value_or_default $Hint $Default);
        $hint_str = "$(sh_colored "darkgray")(${str})";
    }
    
    $a = "$(sh_colored "yellow")?";
    $b = "$(sh_colored "white")${Text}";
    $prompt_str = "${a} ${b} ${hint_str}".Trim();

    while($true) {
        Write-Host -NoNewLine "${prompt_str}${reset_escape} "; ## @notice: trailing space...

        $value = (sh_value_or_default (Read-Host) $Placeholder);

        if($Valid.Count -eq 0) {
            return $value;
        }

        foreach($item in $Valid) {
            if($IgnoreCase) {
                if($item -ieq $value) {
                    return $value;
                }
            } else {
                if($item -eq $value) {
                    return $value;
                }
            }
        }

        if(-not $value -and $EmptyIsValid) {
            return $value;
        }

        if(-not $NoErrorHint) {
            Write-Host -NoNewLine "Invalid Value (${value}) - Expected: ${valid_str}";
            $null = [Console]::ReadKey(); ### @bug (CONTROL+C);

            Write-Host -NoNewLine $up_escape $erase_escape;
        }
    }
}

##------------------------------------------------------------------------------
function sh_ask_confirm()
{
    Param(
        $Text,
        [switch] $DefaultYes
    )

    $hint  = if($DefaultYes) { "Y/n" } else { "y/N" }
    $valid = @("y", "yes", "n", "no");

    $result = sh_ask               `
        -Text         $Text        `
        -Hint         $hint        `
        -Valid        $valid       `
        -NoErrorHint               `
        -EmptyIsValid              `
        -IgnoreCase                `
    ;

    if(-not $result) {
        return $DefaultYes -and $true;
    }

    return ($result[0] -ieq "y")
}


##----------------------------------------------------------------------------##
##                                                                            ##
## String Utils                                                               ##
##                                                                            ##
##----------------------------------------------------------------------------##

##------------------------------------------------------------------------------
$SH_NEW_LINE     = "`n";


##------------------------------------------------------------------------------
## @todo(parameter): Add -KeepSpaces to let the option to not
## strip the arguments that are only spaces...
function sh_join_string()
{
    $separator = $args[0];
    $values    = (sh_expand_array $args 1);

    $output = "";
    foreach($item in $values) {
        $clean = $item.Trim();
        if($clean) {
            $output += "${item}${separator}";
        }
    }

    return $output.TrimEnd($separator);

    ## @improve(string): Join-String joins the empty strings...
    ## return (Join-String -Separator $separator -InputObject $values);
}


##
## MISC
##

function sh_get_user_name() 
{
    if($env:USER) { 
        return $env:USER;
    } else { 
        return $env:UserName;
    }
}

function sh_echo()
{
    param(
        [switch] $NoNewLine
    );

   echo $args;
}

##------------------------------------------------------------------------------
function sh_value_or_default()
{
    if($args[0]) {
        return $args[0];
    }
    return $args[1];
}

##------------------------------------------------------------------------------
function sh_add_quotes()
{
    ## @improve: [Add single quotes] - 22-03-06
    ## if $args[1] is == "single" add single quotes...
    $value = $args[0];
    return "`"${value}`"";
}

##------------------------------------------------------------------------------
function sh_get_temp_filename()
{
    return [System.IO.Path]::GetTempFileName();
}

##------------------------------------------------------------------------------
function sh_date_time_for_filenames()
{
    $fmt = "%Y-%m-%d %H:%M:%S";
    if($args[0] -eq "safe") {
        $fmt = "%Y-%m-%d_%H-%M-%S";
    }

    $str = (Get-Date -UFormat $fmt);
    return $str;
}


# ##------------------------------------------------------------------------------
# ##-----------------------------
# ##-----------------------------
# ##-----------------------------
# $SH_ANSI = @{
#     colors = @{
#         normal = @{
#             fg = @{
#                 black    =  30;
#                 red      =  31;
#                 green    =  32;
#                 yellow   =  33;
#                 blue     =  34;
#                 magenta  =  35;
#                 cyan     =  36;
#                 white    =  37;
#             }
#             bg = @{
#                 black    =  40;
#                 red      =  41;
#                 green    =  42;
#                 yellow   =  43;
#                 blue     =  44;
#                 magenta  =  45;
#                 cyan     =  46;
#                 white    =  47;
#             }
#         }

#         bright = @{
#             fg = @{
#                 black    =  90;
#                 red      =  91;
#                 green    =  92;
#                 yellow   =  93;
#                 blue     =  94;
#                 magenta  =  95;
#                 cyan     =  96;
#                 white    =  97;
#             }
#             bg = @{
#                 black    =  100;
#                 red      =  101;
#                 green    =  102;
#                 yellow   =  103;
#                 blue     =  104;
#                 magenta  =  105;
#                 cyan     =  106;
#                 white    =  107;
#             }
#         }
#     }
# }

##------------------------------------------------------------------------------
function sh_ansi($code)
{
    $ansi = "`e[${code}";
    return $ansi;
}

##------------------------------------------------------------------------------
function sh_ansi_color($fg, $bg)
{
    $result = "$(sh_ansi "$fg;$bg")m";
    return $result;
}

##------------------------------------------------------------------------------
function sh_ansi_color_hex($fg, $bg)
{
    ## @todo(reset flag): -Reset if function should reset....
    $fg_rgb = (_convert_to_rgb_from_hex_str $fg);
    $bg_rgb = (_convert_to_rgb_from_hex_str $bg);

    $fg_ansi = if($fg_rgb) {
        $r = $fg_rgb.r; $g = $fg_rgb.g; $b = $fg_rgb.b;
        "$(sh_ansi "38;2;")${r};${g};${b}m";
    } else { "" };

    $bg_ansi = if($bg_rgb) {
        $r = $bg_rgb.r; $g = $bg_rgb.g; $b = $bg_rgb.b;
        "$(sh_ansi "48;2;")${r};${g};${b}m";
    } else { "" };

    return "${fg_ansi}${bg_ansi}";
}

##------------------------------------------------------------------------------
function _convert_to_rgb_from_hex_str($hex)
{
    if(-not $hex) {
        return $null;
    }

    if($hex[0] -eq "#") {
        $hex = ($hex.SubString(1));
    }

    while($hex.Length -lt 6) {
        $hex += "0";
    }

    try {
        $rgb = @{};
        $rgb.r = [uint32]("#" + $hex[0] + $hex[1]);
        $rgb.g = [uint32]("#" + $hex[2] + $hex[3]);
        $rgb.b = [uint32]("#" + $hex[4] + $hex[5]);
    } catch {
		sh_log_fatal "Failed to convert hex string to rgb!! (${hex})";
    }
    return $rgb;
}

function _hex_from_arg($arg)
{
    if(-not $arg) {
        return "";
    }

    if($arg.StartsWith("#")) {
        return $arg;
    }

    if($arg -eq "red")       { return "#FF0000"; }
    if($arg -eq "green")     { return "#00FF00"; }
    if($arg -eq "blue")      { return "#0000FF"; }
    if($arg -eq "cyan")      { return "#00FFFF"; }
    if($arg -eq "yellow")    { return "#FFFF00"; }
    if($arg -eq "magenta")   { return "#FF00FF"; }
    if($arg -eq "black")     { return "#000000"; }
    if($arg -eq "white")     { return "#FFFFFF"; }
    if($arg -eq "darkgray")  { return "#525252"; }
    if($arg -eq "lightgray") { return "#C2C2C2"; }
}

function sh_colored($fg, $bg)
{
    if(-not ${fg} -and -not ${bg}) {
        (sh_ansi_color 0)
        return;
    }

    $fg_color = (_hex_from_arg $fg);
    $bg_color = (_hex_from_arg $bg);

    (sh_ansi_color_hex $fg_color $bg_color)
}
